import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AddIngredientsAndDeleteProductTest.class, AddToShoppingCartAndCheckTotalPriceTest.class,
		CheckRegistrationDataTest.class, CompanyDataTest.class, LoginTest.class, PerformanceTest.class,
		RegistrationTest.class, ChangeDeliveryMethodTest.class, CheckSocialNetworksTest.class,
		CheckDiscountTest.class })
public class TestSuite {

}
