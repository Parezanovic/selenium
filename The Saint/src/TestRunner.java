import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class TestRunner {

	public static void main(String[] args) throws IOException {

		BufferedWriter writer = new BufferedWriter(new FileWriter("test-report.txt", true));

		Result result = JUnitCore.runClasses();

		writer.newLine();
		result = JUnitCore.runClasses(LoginTest.class);
		writer.append("LoginTest - testiranje logovanja korisnika - " + result.wasSuccessful());
		writer.newLine();
		
		result = JUnitCore.runClasses(CheckSocialNetworksTest.class);
		writer.append("CheckSocialNetworksTest - testiranje društvenih mreža - " + result.wasSuccessful());
		writer.newLine();

		result = JUnitCore.runClasses(RegistrationTest.class);
		writer.append("RegistrationTest - testiranje registracije korisnika - " + result.wasSuccessful());
		writer.newLine();

		result = JUnitCore.runClasses(AddToShoppingCartAndCheckTotalPriceTest.class);
		writer.append(
				"AddToShoppingCartAndCheckTotalPriceTest - testiranje dodavanja u korpu i provere ispravnosti cena - "
						+ result.wasSuccessful());
		writer.newLine();

		result = JUnitCore.runClasses(CheckRegistrationDataTest.class);
		writer.append("CheckRegistrationDataTest - testiranje korisničkih podataka pri registraciji - "
				+ result.wasSuccessful());
		writer.newLine();

		result = JUnitCore.runClasses(CompanyDataTest.class);
		writer.append("CompanyDataTest - testiranje preuzimanja podataka sajta - " + result.wasSuccessful());
		writer.newLine();

		result = JUnitCore.runClasses(PerformanceTest.class);
		writer.append("PerformanceTest - testiranje performansi sajta - " + result.wasSuccessful());
		writer.newLine();

		result = JUnitCore.runClasses(AddIngredientsAndDeleteProductTest.class);
		writer.append("AddIngredientsAndDeleteProduct - testiranje dodavanja sastojaka i brisanje proizvoda iz korpe - "
				+ result.wasSuccessful());
		writer.newLine();

		result = JUnitCore.runClasses(ChangeDeliveryMethodTest.class);
		writer.append("ChangeDeliveryMethodTest - testiranje promene isporuke - " + result.wasSuccessful());
		writer.newLine();

		result = JUnitCore.runClasses(CheckDiscountTest.class);
		writer.append("CheckDiscountTest - testiranje popusta na 'Saint Meal' obrok - " + result.wasSuccessful());
		writer.newLine();

		long meni = PerformanceTest.TOTAL_TIME_MENI;
		long oNama = PerformanceTest.TOTAL_TIME_ONAMA;
		long lokacije = PerformanceTest.TOTAL_TIME_LOKACIJE;
		long zoneDostave = PerformanceTest.TOTAL_TIME_ZONE_DOSTAVE;
		long kontakt = PerformanceTest.TOTAL_TIME_KONTAKT;

		long vremeMS = PerformanceTest.vremeMS;
		long prosekMS = PerformanceTest.prosekMS;
		long vremeS = PerformanceTest.vremeS;
		long prosekS = PerformanceTest.prosekS;

		long početna = PerformanceTest.TOTAL_TIME1;
		long meni2 = PerformanceTest.TOTAL_TIME2;
		long lokacije2 = PerformanceTest.TOTAL_TIME3;
		long zoneDostave2 = PerformanceTest.TOTAL_TIME4;
		long kontakt2 = PerformanceTest.TOTAL_TIME5;
		long onama2 = PerformanceTest.TOTAL_TIME6;

		long total = početna + meni2 + lokacije2 + zoneDostave2 + kontakt2 + onama2;
		long vremeMS2 = total;
		long prosekMS2 = total / 6;
		long vremeS2 = total / 1000;
		long prosekS2 = total / 1000 / 6;

		writer.newLine();
		writer.append("Vreme izvršavanja stranice Meni (samo stranica): " + meni + "ms");
		writer.newLine();
		writer.append("Vreme izvršavanja stranice O Nama (samo stranica): " + oNama + "ms");
		writer.newLine();
		writer.append("Vreme izvršavanja stranice Lokacije (samo stranica): " + lokacije + "ms");
		writer.newLine();
		writer.append("Vreme izvršavanja stranice Zone Dostave (samo stranica): " + zoneDostave + "ms");
		writer.newLine();
		writer.append("Vreme izvršavanja stranice Kontakt (samo stranica): " + kontakt + "ms");
		writer.newLine();
		writer.newLine();
		writer.append("Ukupno vreme otvaranja stranica (samo stranica): " + vremeMS + "ms");
		writer.newLine();
		writer.append("Prosečno vreme otvaranja stranica (samo stranica): " + prosekMS + "ms");
		writer.newLine();
		writer.append("Ukupno vreme otvaranja stranica (samo stranica): " + vremeS + "s");
		writer.newLine();
		writer.append("Prosečno vreme otvaranja stranica (samo stranica): " + prosekS + "s");
		writer.newLine();
		writer.newLine();
		writer.append("Vreme otvaranja stranice Početna (pretraživač + stranica): " + početna + "ms");
		writer.newLine();
		writer.append("Vreme otvaranja stranice Meni (pretraživač + stranica): " + meni2 + "ms");
		writer.newLine();
		writer.append("Vreme otvaranja stranice Lokacije (pretraživač + stranica): " + lokacije2 + "ms");
		writer.newLine();
		writer.append("Vreme otvaranja stranice Zone Dostave (pretraživač + stranica): " + zoneDostave2 + "ms");
		writer.newLine();
		writer.append("Vreme otvaranja stranice Kontakt (pretraživač + stranica): " + kontakt2 + "ms");
		writer.newLine();
		writer.append("Vreme otvaranja stranice O nama (pretraživač + stranica): " + onama2 + "ms");
		writer.newLine();
		writer.newLine();
		writer.append("Ukupno vreme otvaranja stranica (pretraživač + stranica): " + vremeMS2 + "ms");
		writer.newLine();
		writer.append("Prosečno vreme otvaranja stranica (pretraživač + stranica): " + prosekMS2 + "ms");
		writer.newLine();
		writer.append("Ukupno vreme otvaranja stranica (pretraživač + stranica): " + vremeS2 + "s");
		writer.newLine();
		writer.append("Prosečno vreme otvaranja stranica (pretraživač + stranica): " + prosekS2 + "s");
		writer.newLine();

		if (result.wasSuccessful()) {
			System.out.println("Svi testovi su uspešno prošli!");
		} else {
			System.out.println("Neki od testova nisu prošli!");
		}

		writer.close();
	}

}
