
// Generated by Selenium IDE
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.Before;
import org.junit.Rule;
import org.junit.After;
import org.junit.AfterClass;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.util.logging.Logger;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class AddToShoppingCartAndCheckTotalPriceTest {

	private String exePath = "C:\\Users\\PakI\\Desktop\\VII semestar\\3. Testiranje softvera\\operadriver_win64\\\\operadriver.exe";
	private WebDriver driver;
	private Map<String, Object> vars;
	JavascriptExecutor js;

	@Rule
	public final TestName name = new TestName();

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", exePath);
		driver = new ChromeDriver();
		js = (JavascriptExecutor) driver;
		vars = new HashMap<String, Object>();
	}

	@After
	public void tearDown() {
		driver.quit();
	}

	Logger l = Logger.getLogger(LoginTest.class.getName());

	String output = "";
	String testName = "";
	String testDesc = "";
	boolean testPassed = false;

	static double totalPriceDouble, deliveryPriceDouble, totalPriceDeliveryDouble;

	@Test
	public void addToShoppingCartAndCheckTotalPriceTest() throws InterruptedException {
		testName = name.getMethodName();
		testDesc = "Dodavanje tri proizvoda u korpu i izračunavanje cene.";

		driver.get("https://www.thesaint.rs/");
		driver.manage().window().setSize(new Dimension(1382, 744));
		Thread.sleep(11000);
		driver.navigate().refresh();
		{
			WebElement element = driver.findElement(By.cssSelector(".level3 span"));
			Actions builder = new Actions(driver);
			builder.moveToElement(element).perform();
		}
		Thread.sleep(1500);
		driver.findElement(By.cssSelector("li:nth-child(2) h5")).click();
		driver.findElement(By.cssSelector(".cookie-agree")).click();
		Thread.sleep(2500);
		driver.findElement(By.linkText("Pizza")).click();
		Thread.sleep(2500);
		driver.findElement(By.linkText("Poruči")).click();
		Thread.sleep(2500);
		driver.findElement(By.cssSelector(".size-item:nth-child(3)")).click();
		driver.findElement(By.id("nb_addToCartButton")).click();
		Thread.sleep(2500);
		driver.findElement(By.linkText("Paste")).click();
		Thread.sleep(2500);
		driver.findElement(By.cssSelector("#paste .item:nth-child(2) .bottom-btn-wrapper span")).click();
		{
			WebElement element = driver
					.findElement(By.cssSelector("#paste .item:nth-child(2) .bottom-btn-wrapper span"));
			Actions builder = new Actions(driver);
			builder.moveToElement(element).perform();
		}
		{
			WebElement element = driver.findElement(By.tagName("body"));
			Actions builder = new Actions(driver);
			builder.moveToElement(element, 0, 0).perform();
		}
		Thread.sleep(1500);
		driver.findElement(By.cssSelector(".ease:nth-child(3) > span")).click();
		driver.findElement(By.id("nb_addToCartButton")).click();
		Thread.sleep(2500);
		driver.findElement(By.linkText("Obrok Meni")).click();
		js.executeScript("window.scrollTo(0,6743)");
		Thread.sleep(2500);
		driver.findElement(By.cssSelector("#obrok-meni .item:nth-child(2) .quick-view-category")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("nb_addToCartButton")).click();
		Thread.sleep(2500);
		driver.findElement(By.cssSelector(".mini-cart > a")).click();
		Thread.sleep(2500);

		// Check total price
		assertEquals(driver.findElement(By.cssSelector(".total-price:nth-child(1)")).getText(), "Ukupno\n1.430,00 RSD");
		// Check delivery price
		assertEquals(driver.findElement(By.cssSelector(".total-price:nth-child(2)")).getText(),
				"Troškovi dostave\nBesplatna dostava");
		// Check total price with delivery price
		assertEquals(driver.findElement(By.cssSelector(".total-price:nth-child(3)")).getText(),
				"Ukupno za plaćanje sa PDV-om\n1.430,00 RSD");

		String categoryPizza = driver.findElement(By.linkText("Pizza")).getText();
		String categoryPaste = driver.findElement(By.linkText("Paste")).getText();
		String categoryObrakMeni = driver.findElement(By.linkText("Obrok Meni")).getText();

		// Check if the products are from different categories
		assertNotEquals(categoryPizza, categoryPaste);
		assertNotEquals(categoryPaste, categoryObrakMeni);
		assertNotEquals(categoryObrakMeni, categoryPizza);

		// Check if there is 3 products in shopping cart
		assertEquals(driver.findElement(By.cssSelector(".list-title")).getText(), "Imate 3 proizvoda u korpi");

		// Price equality check
		// Check total price
		String totalPriceString = driver.findElement(By.cssSelector(".total-price:nth-child(1)")).getText()
				.replace("Ukupno\n", "").replace(".", "").replace(" RSD", "").replace(',', '.');
		totalPriceDouble = Double.parseDouble(totalPriceString);

		assertEquals(totalPriceDouble, 1430.00, 2);

		// Check delivery price
		String deliveryPriceString = driver.findElement(By.cssSelector(".total-price:nth-child(2)")).getText()
				.replace("Troškovi dostave\nBesplatna dostava", "0");
		deliveryPriceDouble = Double.parseDouble(deliveryPriceString);

		assertEquals(deliveryPriceDouble, 0.00, 2);

		// Check total price with delivery price
		String totalPriceDeliveryString = driver.findElement(By.cssSelector(".total-price:nth-child(3)")).getText()
				.replace("Ukupno za plaćanje sa PDV-om\n", "").replace(".", "").replace(" RSD", "").replace(',', '.');
		totalPriceDeliveryDouble = Double.parseDouble(totalPriceDeliveryString);

		assertEquals(totalPriceDeliveryDouble, 1430.00, 2);

		// Check expected and actual values
		Double expectedResult = totalPriceDeliveryDouble;
		Double actualResult = totalPriceDouble + deliveryPriceDouble;

		assertEquals(expectedResult, actualResult);

		testPassed = true;
	}

	@AfterClass
	public static void Output() {
		System.out.println("Ukupno = " + totalPriceDouble);
		System.out.println("Troškovi dostave = " + deliveryPriceDouble);
		System.out.println("Ukupno + Troškovi dostave = " + totalPriceDeliveryDouble);
	}

	@After
	public void WriteSingleTest() {
		output += "Testiranje metode " + testName + ", u klasi " + this.getClass().getSimpleName() + "\n";
		output += "\n\tNaziv testa: " + testName;
		output += "\n\tOpis testa: " + testDesc;
		output += "\n\tTest je uspešno prošao: " + testPassed + "\n";
		output += "\n";
		l.info(output);
		try {
			FileWriter fw = new FileWriter("test-report2.txt", true);
			fw.write(output);
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
